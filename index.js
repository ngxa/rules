module.exports = {
  defaultSeverity: 'error',
  rulesDirectory: [
    'codelyzer',
    'rxjs-tslint',
    'tslint-misc-rules',
    'tslint-consistent-codestyle',
    '../../rxjs-tslint-rules/dist/rules',
    '../../tslint-etc/dist/rules',
    '../../tslint-sonarts/lib/rules',
    '../../tslint-clean-code/dist/src',
    '../../tslint-eslint-rules/dist/rules',
    '../../tslint-microsoft-contrib'
  ],
  linterOptions: {
    exclude: ['**/*.spec.ts', '**/*-spec.ts', '**/test.ts', '**/*.po.ts']
  },
  rules: {
    /**
     * TSLint core rules v5.18.0
     *
     * Lint rules encode logic for syntactic & semantic checks of TypeScript source code.
     * @link https://palantir.github.io/tslint/rules/
     */

    // TypeScript-specific

    'adjacent-overload-signatures': [true, { 'ignore-accessors': true }],
    'ban-ts-ignore': true,
    'ban-types': {
      options: [
        ['Object', 'Avoid using the `Object` type. Did you mean `object`?'],
        [
          'Function',
          'Avoid using the `Function` type. Prefer a specific function type, like `() => void`.'
        ],
        ['Boolean', 'Avoid using the `Boolean` type. Did you mean `boolean`?'],
        ['Number', 'Avoid using the `Number` type. Did you mean `number`?'],
        ['String', 'Avoid using the `String` type. Did you mean `string`?'],
        ['Symbol', 'Avoid using the `Symbol` type. Did you mean `symbol`?']
      ]
    },
    'member-access': [true, 'no-public'],
    'member-ordering': [
      true,
      {
        order: [
          'public-static-field',
          'protected-static-field',
          'private-static-field',
          'static-method',
          'public-instance-field',
          'protected-instance-field',
          'private-instance-field',
          'public-constructor',
          'protected-constructor',
          'private-constructor',
          'instance-method'
        ]
      }
    ],
    'no-any': true,
    'no-empty-interface': true,
    'no-import-side-effect': [
      true,
      { 'ignore-module': '(angular|angular-material|zone\\.js)' }
    ],
    'no-inferrable-types': false, // conflictive with typedef
    'no-internal-module': true,
    'no-magic-numbers': true,
    'no-namespace': true,
    'no-non-null-assertion': true,
    'no-parameter-reassignment': true,
    'no-reference': true,
    'no-unnecessary-type-assertion': true,
    'no-var-requires': true,
    'only-arrow-functions': [
      true,
      'allow-declarations',
      'allow-named-functions'
    ],
    'prefer-for-of': true,
    'promise-function-async': true,
    typedef: [
      true,
      'call-signature',
      'arrow-call-signature',
      'parameter',
      'arrow-parameter',
      'property-declaration',
      'variable-declaration',
      'member-variable-declaration',
      'object-destructuring',
      'array-destructuring'
    ],
    'unified-signatures': true,

    // Functionality

    'await-promise': true,
    'ban-comma-operator': true,
    ban: false, // TODO
    curly: true,
    forin: true,
    'function-constructor': true,
    'import-blacklist': false, // TODO
    'label-position': true,
    'no-arg': true,
    'no-async-without-await': true,
    'no-bitwise': true,
    'no-conditional-assignment': true,
    'no-console': [true, 'debug', 'info', 'log', 'time', 'timeEnd', 'trace'],
    'no-construct': true,
    'no-debugger': true,
    'no-duplicate-super': true,
    'no-duplicate-switch-case': true,
    'no-duplicate-variable': [true, 'check-parameters'],
    'no-dynamic-delete': true,
    'no-empty': true,
    'no-eval': true,
    'no-floating-promises': true,
    'no-for-in-array': true,
    'no-implicit-dependencies': [true, ['dev']],
    'no-inferred-empty-object-type': true,
    'no-invalid-template-strings': true,
    'no-invalid-this': [true, 'check-function-in-method'],
    'no-misused-new': true,
    'no-null-keyword': false, // null is a valid data type
    'no-null-undefined-union': true,
    'no-object-literal-type-assertion': true,
    'no-restricted-globals': [true, 'name', 'length', 'event'],
    'no-return-await': true,
    'no-shadowed-variable': true,
    'no-sparse-arrays': true,
    'no-string-literal': true,
    'no-string-throw': true,
    'no-submodule-imports': [
      true,
      'zone.js/dist/zone-error',
      'zone.js/dist/zone',
      '@angular/common/http',
      'rxjs/operators'
    ],
    'no-switch-case-fall-through': true,
    'no-tautology-expression': true,
    'no-this-assignment': true,
    'no-unbound-method': true,
    'no-unnecessary-class': [true, 'allow-empty-class', 'allow-static-only'],
    'no-unsafe-any': true,
    'no-unsafe-finally': true,
    'no-unused-expression': true,
    'no-unused-variable': false, // deprecated since TypeScript 2.9. Please use the built-in compiler checks instead.
    'no-use-before-declare': false, // deprecated since TypeScript 2.9. Please use the built-in compiler checks instead.
    'no-var-keyword': true,
    'no-void-expression': true,
    'prefer-conditional-expression': [true, 'check-else-if'],
    'prefer-object-spread': true,
    radix: true,
    'restrict-plus-operands': true,
    'static-this': true,
    'strict-boolean-expressions': true,
    'strict-comparisons': false, // bug: do not allow === undefined | null
    'strict-type-predicates': true,
    'switch-default': true,
    'triple-equals': [true, 'allow-undefined-check'],
    'typeof-compare': false, // deprecated since TypeScript 2.2. The compiler includes this check which makes this rule redundant.
    'unnecessary-constructor': true,
    'use-default-type-parameter': true,
    'use-isnan': true,

    // Maintainability

    'cyclomatic-complexity': [true, 15],
    deprecation: true,
    'max-classes-per-file': [true, 1],
    'max-file-line-count': [true, 400],
    'no-default-export': true,
    'no-default-import': true,
    'no-duplicate-imports': true,
    'no-mergeable-namespace': true,
    'no-require-imports': true,
    'object-literal-sort-keys': false, // Sort keys in a meaningful manner
    'prefer-const': true,
    'prefer-readonly': true,

    // Style

    'array-type': false,
    'arrow-return-shorthand': [true, 'multiline'],
    'binary-expression-operand-order': true,
    'callable-types': true,
    'class-name': true,
    'comment-format': [true, 'check-space'],
    'comment-type': [true, 'doc', 'singleline'],
    'completed-docs': false, // Set to true only in libraries
    encoding: true,
    'file-header': false, // Enable only if you are legally required
    'file-name-casing': [true, 'kebab-case'],
    'increment-decrement': true,
    'interface-name': false, // Avoid hungarian style notation
    'interface-over-type-literal': true,
    'match-default-export-name': true,
    'newline-per-chained-call': false, // Prettier
    'no-angle-bracket-type-assertion': true,
    'no-boolean-literal-compare': true,
    'no-parameter-properties': false, // Injections are done in the constructor
    'no-redundant-jsdoc': true,
    'no-reference-import': true,
    'no-unnecessary-callback-wrapper': true,
    'no-unnecessary-initializer': true,
    'no-unnecessary-qualifier': true,
    'object-literal-key-quotes': false, // Prettier
    'object-literal-shorthand': true,
    'one-line': false, // Prettier
    'one-variable-per-declaration': [true, 'ignore-for-loop'],
    'ordered-imports': false, // Use Typescript Hero
    'prefer-function-over-method': [true, 'allow-public', 'allow-protected'],
    'prefer-method-signature': true,
    'prefer-switch': true,
    'prefer-template': true,
    'prefer-while': true,
    'return-undefined': true,
    'space-before-function-paren': false, // Prettier
    'space-within-parens': false, // Prettier
    'switch-final-break': true,
    'type-literal-delimiter': false, // Prettier
    'unnecessary-bind': true,
    'unnecessary-else': true,
    'variable-name': [
      true,
      'ban-keywords',
      'check-format',
      'allow-leading-underscore'
    ],

    // Format

    align: false, // Prettier
    'arrow-parens': false, // Prettier
    eofline: false, // Prettier
    'import-spacing': false, // Prettier
    indent: false, // Prettier
    'jsdoc-format': [true, 'check-multiline-start'],
    'linebreak-style': false, // Prettier
    'max-line-length': false, // Prettier
    'newline-before-return': true,
    'new-parens': false, // Prettier
    'no-consecutive-blank-lines': false, // Prettier
    'no-irregular-whitespace': false, // Prettier
    'no-trailing-whitespace': false, // Prettier
    'number-literal-format': false, // Prettier
    quotemark: false, // Prettier
    semicolon: false, // Prettier
    'trailing-comma': false, // Prettier
    'typedef-whitespace': false, // Prettier
    whitespace: false, // Prettier

    /**
     * Codelyzer rules v5.1.0
     *
     * Lint rules encode logic for syntactic & semantic checks of TypeScript, HTML, CSS and Angular
     * expressions source code.
     * @link http://codelyzer.com/rules/
     */

    // Functionality

    'contextual-decorator': true,
    'contextual-life-cycle': true,
    'no-attribute-decorator': true,
    'no-lifecycle-call': true,
    'no-output-native': true,
    'no-pipe-impure': true,
    'prefer-on-push-component-change-detection': true,
    'template-accessibility-alt-text': true,
    'template-accessibility-elements-content': true,
    'template-accessibility-label-for': true,
    'template-accessibility-tabindex-no-positive': true,
    'template-accessibility-table-scope': true,
    'template-accessibility-valid-aria': true,
    'template-banana-in-box': true,
    'template-click-events-have-key-events': true,
    'template-mouse-events-have-key-events': true,
    'template-no-any': true,
    'template-no-autofocus': true,
    'template-no-distracting-elements': true,
    'template-no-negated-async': true,
    'use-injectable-provided-in': true,
    'use-lifecycle-interface': true,

    // Maintainability

    'component-max-inline-declarations': true,
    'no-conflicting-lifecycle': true,
    'no-forward-ref': true,
    'no-input-prefix': [true, 'can', 'is', 'should'],
    'no-input-rename': true,
    'no-output-on-prefix': true,
    'no-output-rename': true,
    'no-unused-css': true,
    'prefer-output-readonly': true,
    'relative-url-prefix': true,
    'template-conditional-complexity': [true, 4],
    'template-cyclomatic-complexity': [true, 5],
    'template-i18n': false, // We preffer L10n Angular
    'template-no-call-expression': true,
    'template-use-track-by-function': true,
    'use-component-selector': true,
    'use-component-view-encapsulation': true,
    'use-pipe-decorator': true,
    'use-pipe-transform-interface': true,

    // Style

    'angular-whitespace': false, // Prettier
    'component-class-suffix': [true, 'Component', 'Page'],
    'component-selector': false, // setted in project
    'directive-class-suffix': [true, 'Directive'],
    'directive-selector': false, // setted in project
    'import-destructuring-spacing': false, // Prettier
    'no-host-metadata-property': true,
    'no-inputs-metadata-property': true,
    'no-outputs-metadata-property': true,
    'no-queries-metadata-property': true,
    'pipe-prefix': false, // setted in project
    'prefer-inline-decorator': true,

    /**
     * RxJS TSLint v0.1.7
     *
     * TSLint rules targeting RxJS
     */

    'rxjs-collapse-imports': true,
    'rxjs-no-static-observable-methods': true,
    'rxjs-pipeable-operators-only': true,
    'rxjs-proper-imports': true,

    /**
     * RxJS TSLint rules v4.24.3
     *
     * TSLint rules for RxJS
     */

    // Functionality

    'rxjs-add': false, // Not needed for v6
    'rxjs-ban-observables': false, // TODO
    'rxjs-ban-operators': false, // TODO
    'rxjs-deep-operators': false, // Not needed for v6
    'rxjs-no-add': false, // Not needed for v6
    'rxjs-no-async-subscribe': true,
    'rxjs-no-compat': true,
    'rxjs-no-connectable': true,
    'rxjs-no-create': true,
    'rxjs-no-deep-operators': false, // Not needed for v6
    'rxjs-no-do': true,
    'rxjs-no-explicit-generics': true,
    'rxjs-no-ignored-error': true,
    'rxjs-no-ignored-notifier': true,
    'rxjs-no-ignored-observable': true,
    'rxjs-no-ignored-replay-buffer': true,
    'rxjs-no-ignored-subscribe': true,
    'rxjs-no-ignored-subscription': false, // false fositive with PartialObserver
    'rxjs-no-internal': true,
    'rxjs-no-operator': false, // Not needed for v6
    'rxjs-no-patched': false, // Not needed for v6
    'rxjs-no-sharereplay': true,
    'rxjs-no-subclass': true,
    'rxjs-no-subject-unsubscribe': true,
    'rxjs-no-subject-value': true,
    'rxjs-no-tap': true,
    'rxjs-no-unsafe-catch': true,
    'rxjs-no-unsafe-first': true,
    'rxjs-no-unsafe-scope': true,
    'rxjs-no-unsafe-switchmap': true,
    'rxjs-no-unsafe-takeuntil': true,
    'rxjs-throw-error': true,

    // Maintainability

    'rxjs-no-unbound-methods': false, // Not needed for v6
    'rxjs-no-unused-add': false, // Not needed for v6
    'rxjs-no-wholesale': false, // Not needed for v6

    // Style

    'rxjs-finnish': false, // do not use finnish notation
    'rxjs-just': true,
    'rxjs-no-exposed-subjects': [
      true,
      {
        allowProtected: true
      }
    ],
    'rxjs-no-finnish': true,
    'rxjs-no-nested-subscribe': true,
    'rxjs-prefer-async-pipe': true,
    'rxjs-prefer-observer': true,
    'rxjs-suffix-subjects': false, // I think it's not needed

    /**
     * tslint-etc v1.5.6
     *
     * tslint-etc is set of TSLint rules to enforce policies that cannot be specified - or easily
     * specified - with the built-in rules.
     * @link https://github.com/cartant/tslint-etc
     */

    'ban-imports': false, // TODO
    'no-assign-mutated-array': true,
    'no-const-enum': true,
    'no-enum': true,
    'no-missing-dollar-expect': true,
    'no-unsafe-callback-scope': true,
    'no-unused-declaration': true,
    'throw-error': true,

    /**
     * SonarTS v1.9.0
     *
     * Static code analyzer for TypeScript detecting bugs and suspicious patterns in your code.
     * @link https://github.com/SonarSource/SonarTS
     */

    // Bug Detection

    'no-all-duplicated-branches': true,
    'no-case-with-or': true,
    'no-collection-size-mischeck': true,
    'no-element-overwrite': true,
    'no-empty-array': true,
    'no-empty-destructuring': true,
    'no-identical-conditions': true,
    'no-identical-expressions': true,
    'no-ignored-initial-value': true,
    'no-ignored-return': true,
    'no-in-misuse': true,
    'no-misleading-array-reverse': true,
    'no-misspelled-operator': true,
    'no-self-assignment': true,
    'no-try-promise': true,
    'no-unthrown-error': true,
    'no-use-of-empty-return-value': true,
    'no-useless-increment': true,
    'no-useless-intersection': true,

    // Code Smell Detection

    'arguments-order': true,
    'bool-param-default': true,
    'cognitive-complexity': true,
    'consecutive-overloads': true,
    'max-switch-cases': true,
    'max-union-size': true,
    'mccabe-complexity': true,
    'no-accessor-field-mismatch': true,
    'no-alphabetical-sort': true,
    'no-array-delete': true,
    'no-big-function': true,
    'no-collapsible-if': true,
    'no-commented-code': true,
    'no-dead-store': true,
    'no-duplicate-in-composite': true,
    'no-duplicate-string': true,
    'no-duplicated-branches': true,
    'no-empty-nested-blocks': true,
    'no-extra-semicolon': true,
    'no-gratuitous-expressions': true,
    'no-hardcoded-credentials': true,
    'no-identical-functions': true,
    'no-inconsistent-return': true,
    'no-invalid-await': true,
    'no-invariant-return': true,
    'no-inverted-boolean-check': true,
    'no-multiline-string-literals': true,
    'no-nested-incdec': true,
    'no-nested-switch': true,
    'no-nested-template-literals': true,
    'no-redundant-boolean': true,
    'no-redundant-jump': true,
    'no-redundant-parentheses': true,
    'no-return-type-any': true,
    'no-same-line-conditional': true,
    'no-small-switch': true,
    'no-statements-same-line': true,
    'no-unconditional-jump': true,
    'no-undefined-argument': true,
    'no-unenclosed-multiline-block': true,
    'no-unused-array': true,
    'no-useless-cast': true,
    'no-useless-catch': true,
    'no-variable-usage-before-declaration': true,
    'parameters-max-number': true,
    'prefer-default-last': true,
    'prefer-immediate-return': true,
    'prefer-optional': true,
    'prefer-promise-shorthand': true,
    'prefer-type-guard': true,
    'use-primitive-type': true,
    'use-type-alias': true,

    /**
     * tslint-clean-code v0.2.9
     *
     * A set of TSLint rules used to enforce Clean Code practices. Inspired by Clean Code: A
     * Handbook of Agile Software Craftsmanship.
     * @link https://github.com/Glavin001/tslint-clean-code
     */

    'id-length': [true, ['T', 'E']],
    'max-func-args': [true, 3],
    'min-class-cohesion': [true, 0.5],
    'newspaper-order': true,
    'no-commented-out-code': true,
    'no-complex-conditionals': true,
    'no-feature-envy': true,
    'no-flag-args': true,
    'no-for-each-push': true,
    'no-map-without-usage': true,
    'prefer-dry-conditionals': true,
    'try-catch-first': true,

    /**
     * ESLint rules for TSLint v5.4.0
     *
     * Improve your TSLint with the missing ESLint Rules
     * @link https://github.com/buzinas/tslint-eslint-rules
     */

    'array-bracket-spacing': false, // Prettier
    'block-spacing': false, // Prettier
    'brace-style': false, // Prettier
    'handle-callback-err': [true, 'error'],
    'no-constant-condition': true,
    'no-control-regex': true,
    'no-duplicate-case': true,
    'no-empty-character-class': true,
    'no-ex-assign': true,
    'no-extra-boolean-cast': true,
    'no-extra-semi': false, // Prettier
    'no-inner-declarations': [true, 'both'],
    'no-invalid-regexp': true,
    'no-multi-spaces': false, // Prettier
    'no-regex-spaces': true,
    'no-unexpected-multiline': true,
    'object-curly-spacing': false, // Prettier
    'sort-imports': false, // Use Typescript Hero
    'space-in-parens': false, // Prettier
    'ter-arrow-body-style': [
      true,
      'as-needed',
      {
        requireReturnForObjectLiteral: true
      }
    ],
    'ter-arrow-parens': false, // Prettier
    'ter-arrow-spacing': false, // Prettier
    'ter-computed-property-spacing': false, // Prettier
    'ter-func-call-spacing': false, // Prettier
    'ter-indent': false, // Prettier
    'ter-max-len': false, // Prettier
    'ter-newline-after-var': true,
    'ter-no-irregular-whitespace': false, // Prettier
    'ter-no-mixed-spaces-and-tabs': true,
    'ter-no-proto': true,
    'ter-no-script-url': true,
    'ter-no-self-compare': true,
    'ter-no-sparse-arrays': true,
    'ter-no-tabs': false, // Prettier
    'ter-padded-blocks': false, // Prettier
    'ter-prefer-arrow-callback': true,
    'valid-jsdoc': [
      true,
      {
        prefer: {
          return: 'returns'
        },
        requireReturn: false,
        requireParamType: false,
        requireReturnType: false,
        requireParamDescription: true,
        requireReturnDescription: true
      }
    ],
    'valid-typeof': true,

    /**
     * misc-tslint-rules v3.5.1
     *
     * Collection of miscellaneous TSLint rules
     * @link https://github.com/jwbay/tslint-misc-rules
     */

    'camel-case-local-function': false, // not compliant
    'class-method-newlines': true,
    'declare-class-methods-after-use': true,
    'jsx-attribute-spacing': false, // Prettier
    'jsx-expression-spacing': false, // Prettier
    'jsx-no-braces-for-string-attributes': true,
    'jsx-no-closing-bracket-newline': false, // Prettier
    'no-braces-for-single-line-arrow-functions': false, // not compliant
    'no-property-initializers': false, // prefer initializer
    'no-unnecessary-parens-for-arrow-function-arguments': false, // Prettier
    'prefer-es6-imports': true,
    'prefer-or-operator-over-ternary': false, // prefer ternary
    'react-lifecycle-order': false, // not relevant
    'sort-imports': false, // Use Typescript Hero,

    /**
     * tslint-microsoft-contrib v6.2.0
     *
     * A set of TSLint rules used on some Microsoft projects.
     * @link https://github.com/Microsoft/tslint-microsoft-contrib
     */

    'chai-prefer-contains-to-index-of': false, // not relevant
    'chai-vague-errors': false, // not relevant
    'detect-child-process': true,
    'export-name': false, // module name from Angular
    'function-name': false, // TODO
    'import-name': false, // not relevant
    'informative-docs': true,
    'insecure-random': true,
    'jquery-deferred-must-complete': false, // not relevant
    'max-func-body-length': [
      true,
      {
        'func-body-length': 40,
        'func-expression-body-length': 40,
        'arrow-body-length': 40,
        'method-body-length': 40,
        'ctor-body-length': 40,
        'ignore-comments': true
      }
    ],
    'missing-jsdoc': false, // deprecated
    'missing-optional-annotation': false, // deprecated
    'mocha-avoid-only': false, // not relevant
    'mocha-no-side-effect-code': false, // not relevant
    'mocha-unneeded-done': false, // not relevant
    'no-backbone-get-set-outside-model': true,
    'no-banned-terms': true,
    'no-constant-condition': [true, { checkLoops: false }],
    'no-control-regex': true,
    'no-cookies': true,
    'no-delete-expression': true,
    'no-disable-auto-sanitization': true,
    'no-document-domain': true,
    'no-document-write': true,
    'no-duplicate-case': false, // deprecated
    'no-duplicate-parameter-names': false, // deprecated
    'no-empty-interfaces': false, // deprecated
    'no-empty-line-after-opening-brace': false, // Prettier
    'no-exec-script': true,
    'no-for-in': true,
    'no-function-constructor-with-string-args': false, // deprecated
    'no-function-expression': true,
    'no-http-string': true,
    'no-increment-decrement': false, // deprecated
    'no-inner-html': true,
    'no-invalid-regexp': true,
    'no-jquery-raw-elements': false, // not relevant
    'no-missing-visibility-modifiers': false, // deprecated
    'no-multiline-string': true,
    'no-multiple-var-decl': false, // deprecated
    'non-literal-fs-path': true,
    'non-literal-require': true,
    'no-octal-literal': true,
    'no-regex-spaces': true,
    'no-relative-imports': false, // not relevant
    'no-reserved-keywords': false, // deprecated
    'no-single-line-block-comment': true,
    'no-stateless-class': false, // deprecated
    'no-string-based-set-immediate': true,
    'no-string-based-set-interval': true,
    'no-string-based-set-timeout': true,
    'no-suspicious-comment': true,
    'no-typeof-undefined': true,
    'no-unexternalized-strings': true,
    'no-unnecessary-bind': false, // deprecated
    'no-unnecessary-field-initialization': true,
    'no-unnecessary-local-variable': true,
    'no-unnecessary-override': true,
    'no-unnecessary-semicolons': false, // Prettier
    'no-unsupported-browser-code': true,
    'no-useless-files': true,
    'no-var-self': false, // deprecated
    'no-with-statement': true,
    'possible-timing-attack': true,
    'prefer-array-literal': true,
    'prefer-type-cast': false, // preffer as-cast
    'promise-must-complete': true,
    'react-a11y-accessible-headings': true,
    'react-a11y-anchors': true,
    'react-a11y-aria-unsupported-elements': true,
    'react-a11y-event-has-role': true,
    'react-a11y-iframes': true,
    'react-a11y-image-button-has-alt': true,
    'react-a11y-img-has-alt': true,
    'react-a11y-input-elements': true,
    'react-a11y-lang': true,
    'react-a11y-meta': true,
    'react-a11y-mouse-event-has-key-event': true,
    'react-a11y-no-onchange': true,
    'react-a11y-props': true,
    'react-a11y-proptypes': true,
    'react-a11y-required': true,
    'react-a11y-role': true,
    'react-a11y-role-has-required-aria-props': true,
    'react-a11y-role-supports-aria-props': true,
    'react-a11y-tabindex-no-positive': true,
    'react-a11y-titles': true,
    'react-anchor-blank-noopener': true,
    'react-iframe-missing-sandbox': false, // not relevant
    'react-no-dangerous-html': false, // not relevant
    'react-this-binding-issue': false, // not relevant
    'react-tsx-curly-spacing': false, // Prettier
    'react-unused-props-and-state': false, // not relevant
    'underscore-consistent-invocation': false, // not relevant
    'use-named-parameter': true,
    'use-simple-attributes': true,
    'valid-typeof': false, // deprecated
    'void-zero': true,

    /**
     * tslint-consistent-codestyle v1.15.1
     *
     * The rules in this package can be used to enforce consistent code style.
     * @link https://github.com/ajafff/tslint-consistent-codestyle
     */

    'const-parameters': false, // not relevant
    'early-exit': true,
    'ext-curly': false, // Prettier
    'naming-convention': [
      true,
      {
        type: 'default',
        format: 'camelCase',
        leadingUnderscore: 'forbid',
        trailingUnderscore: 'forbid'
      },
      {
        type: 'variable',
        modifiers: ['export', 'const'],
        format: 'UPPER_CASE'
      },
      { type: 'member', modifiers: 'private', leadingUnderscore: 'require' },
      { type: 'method', filter: '^toJSON$', format: null },
      {
        type: 'property',
        modifiers: ['public', 'static', 'readonly'],
        format: 'UPPER_CASE'
      },
      { type: 'type', format: 'PascalCase' },
      { type: 'class', modifiers: 'abstract', suffix: 'Abstract' },
      { type: 'genericTypeParameter', regex: '^[TKUVER]$' },
      { type: 'enumMember', format: 'PascalCase' }
    ],
    'no-accessor-recursion': true,
    'no-as-type-assertion': false, // prefer as type assertion
    'no-collapsible-if': true,
    'no-else-after-return': true,
    'no-return-undefined': true,
    'no-static-this': true,
    'no-unnecessary-else': true,
    'no-unnecessary-type-annotation': false, // All are necessary
    'no-unused': true,
    'no-var-before-return': true,
    'object-shorthand-properties-first': true,
    'parameter-properties': [true, 'all-or-none', 'member-access', 'readonly'],
    'prefer-const-enum': true,
    'prefer-while': true
  }
};
