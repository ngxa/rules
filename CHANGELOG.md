## [1.7.5](https://gitlab.com/ngxa/rules/compare/v1.7.4...v1.7.5) (2019-07-13)


### Bug Fixes

* **rules:** fix body lenght ([0a2e54f](https://gitlab.com/ngxa/rules/commit/0a2e54f))

## [1.7.4](https://gitlab.com/ngxa/rules/compare/v1.7.3...v1.7.4) (2019-07-11)


### Bug Fixes

* **rules:** change rules settings ([4e9d9f4](https://gitlab.com/ngxa/rules/commit/4e9d9f4))

## [1.7.3](https://gitlab.com/ngxa/rules/compare/v1.7.2...v1.7.3) (2019-07-07)


### Bug Fixes

* **rule:** set valid-jsdoc options ([35ec4d2](https://gitlab.com/ngxa/rules/commit/35ec4d2))
* **rules:** add no-as-type-assertion settings ([909fdbe](https://gitlab.com/ngxa/rules/commit/909fdbe))
* **rules:** change no-submodule-imports settings ([28e94df](https://gitlab.com/ngxa/rules/commit/28e94df))
* **rules:** change rxjs-no-ignored-subscription settings ([43c13d2](https://gitlab.com/ngxa/rules/commit/43c13d2))
* **rules:** change strict-comparison settings ([17b066f](https://gitlab.com/ngxa/rules/commit/17b066f))

## [1.7.2](https://gitlab.com/ngxa/rules/compare/v1.7.1...v1.7.2) (2019-07-06)


### Bug Fixes

* **rules:** fix rules directories ([5c9d86a](https://gitlab.com/ngxa/rules/commit/5c9d86a))

## [1.7.1](https://gitlab.com/ngxa/rules/compare/v1.7.0...v1.7.1) (2019-07-06)


### Bug Fixes

* **rules:** fix no-implicit-dependencies ([6173fe6](https://gitlab.com/ngxa/rules/commit/6173fe6))

# [1.7.0](https://gitlab.com/ngxa/rules/compare/v1.6.2...v1.7.0) (2019-07-06)


### Features

* **rules:** update rules ([5510641](https://gitlab.com/ngxa/rules/commit/5510641))

## [1.6.2](https://gitlab.com/ngxa/rules/compare/v1.6.1...v1.6.2) (2018-12-24)


### Bug Fixes

* **rules:** update rules ([434db37](https://gitlab.com/ngxa/rules/commit/434db37))

## [1.6.1](https://gitlab.com/ngxa/rules/compare/v1.6.0...v1.6.1) (2018-11-21)


### Bug Fixes

* **package:** update versions ([88a4697](https://gitlab.com/ngxa/rules/commit/88a4697))

# [1.6.0](https://gitlab.com/ngxa/rules/compare/v1.5.1...v1.6.0) (2018-10-09)


### Bug Fixes

* **rules:** updated rules ([2ff8c14](https://gitlab.com/ngxa/rules/commit/2ff8c14))


### Features

* **rules:** add new SonarTS rules ([0ddaa02](https://gitlab.com/ngxa/rules/commit/0ddaa02))

## [1.5.1](https://gitlab.com/ngxa/rules/compare/v1.5.0...v1.5.1) (2018-07-26)


### Bug Fixes

* **rules:** activate no-empty-nested-blocks ([4b5a7e4](https://gitlab.com/ngxa/rules/commit/4b5a7e4))
* **rules:** addapt to microsoft and clean code recommendations ([ecbe04d](https://gitlab.com/ngxa/rules/commit/ecbe04d))

# [1.5.0](https://gitlab.com/ngxa/rules/compare/v1.4.3...v1.5.0) (2018-07-24)


### Features

* **rules:** add SonarTS support ([96d4544](https://gitlab.com/ngxa/rules/commit/96d4544))

## [1.4.3](https://gitlab.com/ngxa/rules/compare/v1.4.2...v1.4.3) (2018-07-24)


### Bug Fixes

* **rules:** activate no-multiline-string and no-unexternalized-strings ([5cbe2d5](https://gitlab.com/ngxa/rules/commit/5cbe2d5))

## [1.4.2](https://gitlab.com/ngxa/rules/compare/v1.4.1...v1.4.2) (2018-07-23)


### Bug Fixes

* **rules:** fix deprecated typeof-compare, id-length and remove mscontrib fix ([33f2c6e](https://gitlab.com/ngxa/rules/commit/33f2c6e))

## [1.4.1](https://gitlab.com/ngxa/rules/compare/v1.4.0...v1.4.1) (2018-07-23)


### Bug Fixes

* **rules:** fix rules ([70ec723](https://gitlab.com/ngxa/rules/commit/70ec723))

# [1.4.0](https://gitlab.com/ngxa/rules/compare/v1.3.0...v1.4.0) (2018-07-23)


### Bug Fixes

* **rules:** fix valid-jsdoc rule ([785e424](https://gitlab.com/ngxa/rules/commit/785e424))


### Features

* **rules:** add microsoft contrib and clean code ([5b7647d](https://gitlab.com/ngxa/rules/commit/5b7647d))

# [1.3.0](https://gitlab.com/ngxa/rules/compare/v1.2.3...v1.3.0) (2018-07-22)


### Bug Fixes

* **rules:** fix merge ([cab745b](https://gitlab.com/ngxa/rules/commit/cab745b))


### Features

* **dependencies:** add dependencies to the library ([0746dce](https://gitlab.com/ngxa/rules/commit/0746dce))
* **rules:** add TSLint-ESLint rules ([1a29003](https://gitlab.com/ngxa/rules/commit/1a29003))

## [1.2.3](https://gitlab.com/ngxa/rules/compare/v1.2.2...v1.2.3) (2018-07-20)


### Bug Fixes

* **rules:** add ban to lodash and underscore rules ([09e6995](https://gitlab.com/ngxa/rules/commit/09e6995))

## [1.2.2](https://gitlab.com/ngxa/rules/compare/v1.2.1...v1.2.2) (2018-07-20)


### Bug Fixes

* **rules:** fix rulesDirectory import and prefer-inline-decorator rule values ([ed77bd6](https://gitlab.com/ngxa/rules/commit/ed77bd6))

## [1.2.1](https://gitlab.com/ngxa/rules/compare/v1.2.0...v1.2.1) (2018-07-19)


### Bug Fixes

* **extends:** add rulesDirectory and extends ([0441c69](https://gitlab.com/ngxa/rules/commit/0441c69))
* **rules:** override rules ([6b352d4](https://gitlab.com/ngxa/rules/commit/6b352d4))

# [1.2.0](https://gitlab.com/ngxa/rules/compare/v1.1.0...v1.2.0) (2018-07-19)


### Features

* **codelyzer:** activate angular-whitespace ([dd5e2b6](https://gitlab.com/ngxa/rules/commit/dd5e2b6))
* **rules:** add RxJS rules ([ef809f1](https://gitlab.com/ngxa/rules/commit/ef809f1))

# [1.1.0](https://gitlab.com/ngxa/rules/compare/v1.0.1...v1.1.0) (2018-07-17)


### Features

* **codelyzer:** add codelyzer rules ([93a07ef](https://gitlab.com/ngxa/rules/commit/93a07ef))
* **tslint:** specified new rules and exceptions ([7a15e43](https://gitlab.com/ngxa/rules/commit/7a15e43))

## [1.0.1](https://gitlab.com/ngxa/rules/compare/v1.0.0...v1.0.1) (2018-07-08)


### Bug Fixes

* **rules:** add new rules config ([f8204ba](https://gitlab.com/ngxa/rules/commit/f8204ba))

# 1.0.0 (2018-06-10)


### Features

* **rules:** initial rules config ([4169a60](https://gitlab.com/ngxa/rules/commit/4169a60))
