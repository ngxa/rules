# @ngxa/rules

[![npm](https://img.shields.io/npm/v/@ngxa/rules.svg)](https://www.npmjs.com/package/@ngxa/rules)
[![License: MIT](https://img.shields.io/npm/l/@ngxa/rules.svg)](https://gitlab.com/ngxa/rules/blob/master/LICENSE)

Angular rules library to enforce a consistent code style.

## Installation

You can install `@ngxa/rules` using npm.

```shell
npm install --save-dev tslint@"^5.11.0" @ngxa/rules
```

## Usage

To use these rules, use configuration inheritance via the extends keyword.

A sample configuration is shown below, where `tslint.json` lives adjacent to your `node_modules` folder:

```json
{
  "extends": ["@ngxa/rules"],
  "rules": {
    // override tslint rules here
  }
}
```
